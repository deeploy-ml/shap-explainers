import { Component, Input, OnChanges } from '@angular/core';
import { CaptumTextElement } from '../common/captum-text-element.interface';
import { CaptumTextCss } from '../common/captum-text-css.interface';

@Component({
  selector: 'captum-text',
  templateUrl: './captum-text.component.html',
  styleUrls: ['./captum-text.component.scss'],
})
export class CaptumTextComponent implements OnChanges {
  @Input() word_attributions: number[] = null;
  @Input() raw_input_ids: string[] = null;
  @Input() attr_class?: string = null;
  @Input() attr_score?: number = null;
  @Input() pred_class?: string = null;

  public wordColorsCss: CaptumTextCss[];
  public explanationText: CaptumTextElement[];

  ngOnChanges() {
    this.explanationText = this.formatWordImportances(
      this.raw_input_ids,
      this.word_attributions
    );
    if (this.attr_score !== null) {
      this.attr_score = this.roundToThreeDecimalsIfNeeded(this.attr_score);
    }

    this.wordColorsCss = this.explanationText.map((word) => {
      return this.getStyleText(word);
    });
  }

  public roundToThreeDecimalsIfNeeded(num: number): number {
    return Math.round(num * 1000) / 1000;
  }

  public getStyleText(explanationText: CaptumTextElement): CaptumTextCss {
    const color: CaptumTextCss = {
      'background-color': explanationText.color,
    };
    return color;
  }

  public formatWordImportances(
    words: string[],
    importances: number[] | null
  ): CaptumTextElement[] {
    if (importances === null || importances.length === 0) {
      return [];
    }
    if (words.length > importances.length) {
      throw new Error(
        'Length of words should be less than or equal to the length of importances'
      );
    }
    const textEntries: CaptumTextElement[] = [];
    for (let i = 0; i < words.length; i++) {
      const word = this.formatSpecialTokens(words[i]);
      const importance = importances[i];
      const color = this.getColor(importance);
      const textEntry: CaptumTextElement = {
        word: word,
        color: color,
      };
      textEntries.push(textEntry);
    }
    return textEntries;
  }

  public formatSpecialTokens(token: string): string {
    if (token.startsWith('<') && token.endsWith('>')) {
      return '#' + this.stripChars(token, '<>');
    } else {
      return token;
    }
  }

  public stripChars(str: string, char: string): string {
    return str.replace(
      new RegExp('^[' + char + ']+|[' + char + ']+$', 'g'),
      ''
    );
  }

  public getColor(attr: number): string {
    // clip values to prevent CSS errors (Values should be from [-1,1])
    attr = Math.max(-1, Math.min(1, attr));
    if (attr > 0) {
      const hue = 144;
      const sat = 43;
      const lig = 100 - Math.floor(50 * attr);
      return `hsl(${hue}, ${sat}%, ${lig}%)`;
    } else {
      const hue = 12;
      const sat = 90;
      const lig = 100 - Math.floor(-40 * attr);
      return `hsl(${hue}, ${sat}%, ${lig}%)`;
    }
  }
}
