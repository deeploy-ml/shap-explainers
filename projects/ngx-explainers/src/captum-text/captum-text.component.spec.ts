import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaptumTextComponent } from './captum-text.component';

describe('CaptumTextComponent', () => {
  let component: CaptumTextComponent;
  let fixture: ComponentFixture<CaptumTextComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CaptumTextComponent]
    });
    fixture = TestBed.createComponent(CaptumTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
