import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaptumImageComponent } from './captum-image.component';

describe('CaptumImageComponent', () => {
  let component: CaptumImageComponent;
  let fixture: ComponentFixture<CaptumImageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CaptumImageComponent]
    });
    fixture = TestBed.createComponent(CaptumImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
