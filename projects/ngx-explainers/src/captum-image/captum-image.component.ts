import { Component, Input } from '@angular/core';
import { SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'captum-image',
  templateUrl: './captum-image.component.html',
  styleUrls: ['./captum-image.component.scss'],
})
export class CaptumImageComponent {
  @Input() prediction?: string = null;
  @Input() original?: string | SafeResourceUrl = null;
  @Input() explanation?: string | SafeResourceUrl = null;
}
