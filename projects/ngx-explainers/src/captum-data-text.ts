export interface CaptumTextData {
    word_attributions?:  number[];
    attr_class?: string;
    attr_score?: number;
    raw_input_ids?: string[];
}