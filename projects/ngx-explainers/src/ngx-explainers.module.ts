import { NgModule } from '@angular/core';
import { ShapAdditiveForceArrayComponent } from './shap-additive-force-array/shap-additive-force-array.component';
import { ShapAdditiveForceComponent } from './shap-additive-force/shap-additive-force.component';
import { ShapInfluenceComponent } from './shap-influence/shap-influence.component';
import { CaptumTextComponent } from './captum-text/captum-text.component';
import { CommonModule } from '@angular/common';
import { CaptumImageComponent } from './captum-image/captum-image.component';

@NgModule({
  declarations: [
    ShapAdditiveForceComponent,
    ShapAdditiveForceArrayComponent,
    ShapInfluenceComponent,
    CaptumTextComponent,
    CaptumImageComponent,
  ],
  imports: [CommonModule],
  exports: [
    ShapAdditiveForceComponent,
    ShapAdditiveForceArrayComponent,
    ShapInfluenceComponent,
    CaptumTextComponent,
    CaptumImageComponent,
  ],
})
export class NgxExplainersModule {}
