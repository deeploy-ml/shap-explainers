import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxExplainersModule } from 'projects/ngx-explainers/src/public-api';
import { ExplainersComponent } from './explainers/explainers.component';

@NgModule({
  declarations: [AppComponent, ExplainersComponent],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    NgxExplainersModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
