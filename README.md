# Explainers

This project contains the Angular components for the visual explainers:
1. Shap Additive Force plot
2. Shap Additive Force Array plot
3. Custom: Shap Influence plot
4. Captum Image
5. Captum Text

The project is based on the original code by https://github.com/slundberg/shap and [Captum](https://github.com/pytorch/captum).

**Help us improve this project!**

We want to improve and expand on the project. Help us learn how to do that best by filling out [this survey](https://forms.gle/8yzNiG1admr1N4WV9)

&nbsp;
## Installation

1. Run `npm i ngx-explainers` 
2. Add `NgxExplainersModule` to the imports array
3. Use the one of the following component selectors:
    - `<shap-additive-force>`
    - `<shap-additive-force-array>`
    - `<shap-influence>`
    - `<captum-image>`
    - `<captum-text>`

&nbsp;
## API - How to use the components
  
### shap-additive-force component input parameters:

Changing the colors of the plot. Requires an array of 2 colors.\
`plotColors: string[] = ['rgb(222, 53, 13)', 'rgb(111, 207, 151)'];`

Changing the x-axis type between log-odds (identity) or probabilities (logit).\
`link: 'logit' | 'identity' = 'identity';`

Setting the base value (middle point) for the plot:\
`baseValue: number = 0.0;`

Set the label(s) for the output variables\
`outNames: string[] = ['Color rating'];`

Hide the plot bars:\
`hideBars: boolean = false;`

Set the margin for the labels (labels show up when hovering the bars when not enough space to display the labels)\
`labelMargin: number = 0;`

Hide the label attached to the base value\
`hideBaseValueLabel: boolean = false;`

The data with the feature names and feature values\
`data: AdditiveForceData;`


&nbsp;
### shap-additive-force-array component input parameters:

Set the offset from the top\
`topOffset: number = 28;`

Set the offset from the left\
`leftOffset: number = 80;`

Set the offset from the right\
`rightOffset: number = 10;`

Set the height of the graph\
`height: number = 350;`

Changing the colors of the plot. Requires an array of 2 colors.\
`plotColors: string[] = ['rgb(222, 53, 13)', 'rgb(111, 207, 151)'];`

Changing the x-axis type between log-odds (identity) or probabilities (logit).\
`link: 'logit' | 'identity' = 'identity';`

Setting the base value (middle point) for the plot:\
`baseValue: number = 0.0;`

Set the label(s) for the output variables\
`outNames: string[] = ['Color rating'];`

The data with the feature names and feature values\
`data: AdditiveForceArrayData`


&nbsp;
### shap-influence component input parameters:

Changing the colors of the signs (+/-). Requires an array of 2 colors.\
`influenceColors: string[] = ['rgb(222, 53, 13)', 'rgb(111, 207, 151)'];`

Set the prediction values\
`predictions: number[] = [1];`

Set the prediction label names\
`predictionNames: string[] = ['Income']`

Set the amount of influence labels that are being displayed\
`labelAmount: number = 7;`

&nbsp;
### captum-image component input parameters:
Specify Explanation base64 image (string | SafeResourceUrl)\
`explanation: string|SafeUrl = 'data:image/png;base64,iVBOR...'`

[Optional] Specify Original base64 image (string | SafeResourceUrl)\
`original: string|SafeUrl = 'data:image/png;base64,iVBOR...'`

[Optional] Specify predicted class of image (string)\
`prediction: string = 'cat'`

&nbsp;
### captum-text component input parameters:

Specify word attribution score (number[])\
`word_attributions:  number[] = [0.1, 0.9]`

Specify raw input ids (words) (string[])\
`raw_input_ids: string[] = ['So', 'amazing']`

[Optional] Specify attribution class (string)\
`attr_class: string = 'Positive'`

[Optional] Specify attribution score (number)\
`attr_score: number = '0.9'`
  
[Optional] Specify predicted class (string)\
`pred_class: string = 'Positive'`

&nbsp;
## Interfaces

```
AdditiveForceData {
    featureNames: {
        [key: string]: string;
    };
    features: {
        [key: string]: { [key: string]: number };
    };
}

AdditiveForceArrayData {
    featureNames: {
        [key: string]: string;
    };
    explanations: {
        outValue: number;
        simIndex: number;
        features: {
            [key: string]: { value: number; effect: number; ind?: number };
        };
    }[];
}

InfluenceData {
    featureNames: {
        [key: string]: string;
    };
    valueNames: {
        [key: string]: string;
    };
    features: {
        [key: string]: { [key: string]: number };
    };
}
```


&nbsp;
## Repository

[Deeploy-ml/ngx-explainers](https://gitlab.com/deeploy-ml/ngx-explainers)
